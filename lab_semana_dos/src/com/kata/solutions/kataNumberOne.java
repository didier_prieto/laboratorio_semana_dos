package com.kata.solutions;

public class kataNumberOne {
    public static String solution(String str) {
        String reversedString = "";
        int longStr = str.length() - 1;
        for (int i = 0; i <= longStr; i++) {
            reversedString = reversedString + str.charAt(longStr - i);
        }
        return reversedString;
    }
}
