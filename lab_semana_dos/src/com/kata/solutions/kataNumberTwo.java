package com.kata.solutions;

public class kataNumberTwo {
    public static String winner(String[] deckSteve, String[] deckJosh) {
        int stevesScore = 0;
        int joshScore = 0;
        int count = 0;
        String result = "Tie";
        while (count < deckSteve.length) {
            if (getCardValue(deckSteve[count]) > getCardValue(deckJosh[count])) {
                stevesScore++;
            }
            else if(getCardValue(deckSteve[count]) < getCardValue(deckJosh[count])) {
                joshScore++;
            }
            count++;
        }
        if (stevesScore > joshScore) {
            result = "Steve wins " + stevesScore + " to " + joshScore;
        } else if (stevesScore < joshScore) {
            result = "Josh wins " + joshScore + " to " + stevesScore;
        }
        return result;
    }

    public static  int getCardValue(String card) {
        String[] cardRank = {"2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "Q", "K", "A"};
        int cardValue = 0;
        for (int i = 0; i < cardRank.length; i++) {
            if (card.equals(cardRank[i])) {
                cardValue = i + 1;
            }
        }
        return cardValue;
    }
}
