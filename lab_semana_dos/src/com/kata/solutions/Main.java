package com.kata.solutions;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        kataNumberOne.solution("world");
        System.out.println("Kata 1:" + kataNumberOne.solution("world"));
        System.out.println();
        System.out.println("Kata 2:" + kataNumberTwo.winner(new String[]{"A", "7", "8"}, new String[]{"K", "5", "9"}));
    }
}